package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GameStatisticService {
	@Autowired
	GameStatisticRepository repo;

	public void add(GameStatistic g) {
		repo.save(g);
	}

	public String statistic() {
		Long gamesPlayed = repo.count();
		int humanWon = repo.findByHumanWonTrue().size();
		int computerWon = repo.findByComputerWonTrue().size();
		int draw = repo.findByDrawTrue().size();
		return " gamesPlayed=" + gamesPlayed + " humanWon=" + humanWon + " computerWon=" + computerWon + " draw=" + draw
				+ " ";
	}
}
