package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

@ShellComponent
public class GameCommands {

    private final GameService service;
    private final GameStatisticService statistics;

    @Autowired
    public GameCommands(GameService service, GameStatisticService statistics) {
      this.service = service;
      this.statistics = statistics;
    }

    @ShellMethod("Play rock paper scissors")
    public String play(
      @ShellOption GameOption option
    ) {
    	GameStatistic g = service.play(option);
    	statistics.add(g);
    	return g.toString();
    }
    @ShellMethod("Statistics")
    public String statistics() {
    	return statistics.statistic();
    }
}