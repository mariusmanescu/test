package com.example.demo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class GameStatistic {
	@Id
	@GeneratedValue
	private Long id;
	
	private GameOption human;
	private GameOption computer;
	private Boolean humanWon;
	private Boolean computerWon;
	private Boolean draw;

	public GameStatistic() {
		// TODO Auto-generated constructor stub
	}
	public GameStatistic(GameOption human, GameOption computer, Boolean humanWon, Boolean computerWon, Boolean draw) {
		super();
		this.human = human;
		this.computer = computer;
		this.humanWon = humanWon;
		this.computerWon = computerWon;
		this.draw = draw;
	}

	@Override
	public String toString() {
		return "GameStatistic [id=" + id + ", human=" + human + ", computer=" + computer + ", humanWon=" + humanWon
				+ ", computerWon=" + computerWon + ", draw=" + draw + "]";
	}
	
}
