package com.example.demo;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.springframework.stereotype.Service;


@Service
public class GameService {
	private static final List<GameOption> VALUES = Collections.unmodifiableList(Arrays.asList(GameOption.values()));
	private static final int SIZE = VALUES.size();
	private static final Random RANDOM = new Random();

	private static GameOption randomOption() {
		return VALUES.get(RANDOM.nextInt(SIZE));
	}

	public int compare(GameOption human, GameOption computer) {
		final int humanO = human.ordinal();
		final int computerO = computer.ordinal();
		final int maxO = GameOption.values().length-1;
		final int minO = 0;
		// if equal
		if (humanO == computerO) {
			// draw
			return 0;
		}
		// if human is last in enum and computer is first
		if ((maxO == humanO) && (minO == computerO))
		{
			//computer wins
			return -1;
		}
		// if computer is last in enum and human is first
		if ((maxO == computerO) && (minO == humanO)) {
			// human wins
			return 1;
		}
		// for all other cases the order of the enum gives the winning info
		return humanO > computerO ? 1 : -1;
	}

	public GameStatistic play(GameOption human) {
		GameOption computer = randomOption();
		int result = compare(human, computer);
		GameStatistic g;
		switch (result) {
		case 0: {
			g = new GameStatistic(human, computer, Boolean.FALSE, Boolean.FALSE, Boolean.TRUE);
			break;
		}
		case 1: {
			g = new GameStatistic(human, computer, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE);
			break;
		}
		case -1:{
			g = new GameStatistic(human, computer, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE);
			break;
		}
		default:{
			g = new GameStatistic(human, computer, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE);
		}
		}
//		return " " + human + " " + computer + " " + " " + result + " " +g;
		return g;
	}
}
