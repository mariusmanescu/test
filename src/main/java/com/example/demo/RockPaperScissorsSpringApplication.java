package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/*
shell:>play ROCK
GameStatistic [id=1, human=ROCK, computer=SCISSORS, humanWon=true, computerWon=false, draw=false]
shell:>play ROCK
GameStatistic [id=2, human=ROCK, computer=SCISSORS, humanWon=true, computerWon=false, draw=false]
shell:>play ROCK
GameStatistic [id=3, human=ROCK, computer=PAPER, humanWon=false, computerWon=true, draw=false]
shell:>play ROCK
GameStatistic [id=4, human=ROCK, computer=SCISSORS, humanWon=true, computerWon=false, draw=false]
shell:>play ROCK
GameStatistic [id=5, human=ROCK, computer=SCISSORS, humanWon=true, computerWon=false, draw=false]
shell:>play PAPER
GameStatistic [id=6, human=PAPER, computer=ROCK, humanWon=true, computerWon=false, draw=false]
shell:>play PAPER
GameStatistic [id=7, human=PAPER, computer=PAPER, humanWon=false, computerWon=false, draw=true]
shell:>play PAPER
GameStatistic [id=8, human=PAPER, computer=PAPER, humanWon=false, computerWon=false, draw=true]
shell:>play PAPER
GameStatistic [id=9, human=PAPER, computer=SCISSORS, humanWon=false, computerWon=true, draw=false]
shell:>play PAPER
GameStatistic [id=10, human=PAPER, computer=PAPER, humanWon=false, computerWon=false, draw=true]
shell:>play SCISSORS
GameStatistic [id=11, human=SCISSORS, computer=PAPER, humanWon=true, computerWon=false, draw=false]
shell:>play SCISSORS
GameStatistic [id=12, human=SCISSORS, computer=ROCK, humanWon=false, computerWon=true, draw=false]
shell:>play SCISSORS
GameStatistic [id=13, human=SCISSORS, computer=SCISSORS, humanWon=false, computerWon=false, draw=true]
shell:>play SCISSORS
GameStatistic [id=14, human=SCISSORS, computer=ROCK, humanWon=false, computerWon=true, draw=false]
shell:>play SCISSORS
GameStatistic [id=15, human=SCISSORS, computer=SCISSORS, humanWon=false, computerWon=false, draw=true]
shell:>statistics
2018-06-22 11:36:03.767  INFO 10072 --- [           main] o.h.h.i.QueryTranslatorFactoryInitiator  : HHH000397: Using ASTQueryTranslatorFactory
 gamesPlayed=15 humanWon=6 computerWon=4 draw=5 
 */
@SpringBootApplication
public class RockPaperScissorsSpringApplication {
	public static void main(String[] args) {
		SpringApplication.run(RockPaperScissorsSpringApplication.class, args);
	}
}
