package com.example.demo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface GameStatisticRepository extends CrudRepository<GameStatistic, Long> {
	List<GameStatistic> findByHumanWonTrue();
	List<GameStatistic> findByComputerWonTrue();
	List<GameStatistic> findByDrawTrue();
}